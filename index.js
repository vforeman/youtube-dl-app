'use strict';
const electron = require('electron');

const app = electron.app;

// Module for comm to render process
const {ipcMain} = electron;
const {webFrame} = electron;

// adds debug features like hotkeys for triggering dev tools and reload
require('electron-debug')();

// prevent window being garbage collected
let mainWindow;

function onClosed() {
	// dereference the window
	// for multiple windows store them in an array
	mainWindow = null;
}

function createMainWindow() {
	const win = new electron.BrowserWindow({
		name: "Youtube-DL App",
    width: 450,
    height: 150,
    transparent: true,
    frame: false,
    fullscreen: false,
		titleBarStyle: 'hidden',
    toolbar: false,
		resizable:false,
		maximizable: false,
		minimizable: false,
	});

	win.loadURL(`file://${__dirname}/index.html`);
	//set the position off to the left
	win.setPosition(0,0,true)
	// win.isFullScreenable(true);
	win.setAlwaysOnTop(true);
	// win.webFrame.setZoomLevelLimits(1,1);
	// Open the DevTools.
	win.webContents.openDevTools();
	win.on('closed', onClosed);

	return win;
}

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	if (!mainWindow) {
		mainWindow = createMainWindow();
	}
});

app.on('ready', () => {
	mainWindow = createMainWindow();
});

ipcMain.on('asynchronous-message', (event, arg) => {
  console.log(arg);
  event.sender.send('asynchronous-reply', 'pong');
});
