var gulp = require('gulp'),
run = require('gulp-run'),
electron = require('electron-connect').server.create();

var htmlSources = [
  './src/**/*.html',
  './index.html'
];


gulp.task('serve',()=>{
  electron.start();
  gulp.watch(htmlSources,electron.restart);
})
//
// gulp.task('winstaller',()=>{
//   console.log('sup')
//   var resultPromise = electronWinstaller.createWindowsInstaller({
//     appDirectory: './wearemarcus-win32-x64',
//     outputDirectory: './build/installer64',
//     authors: 'Victor Foreman',
//     exe: 'wearemarcus.exe'
//   });
//   resultPromise
//     .then(() => {
//       console.log("It worked!")
//     }).catch((e) => {
//       console.log(`No dice: ${e.message}`)
//     })
// })



gulp.task('default',['watch','run']);
