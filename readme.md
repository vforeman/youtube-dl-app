# youtube-dl-app

> My sensational app


## Dev

```
$ npm install
```

### Run

```
$ npm start
```

### Build

```
$ npm run build
```

Builds the app for macOS, Linux, and Windows, using [electron-packager](https://github.com/electron-userland/electron-packager).


## License

MIT © [victor foreman](https://bitbucket.org/vforeman/youtube-dl-app)
